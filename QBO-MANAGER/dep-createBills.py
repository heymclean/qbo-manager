import json
import random
import ast
from datetime import date
from dateutil.relativedelta import relativedelta

vendors = open("./storage/vendorIDs.json", "r")
vendorList = ast.literal_eval(vendors.read())

print(type(vendorList))

amountList = []

for item in vendorList:
    amountList.append(str(random.randrange(199,3999)))

pairs = list(zip(vendorList, amountList))

print('-------------------------------')

item = "1"

body = json.dumps(
{"SyncToken": "0", 
"Id": item})

print(body)

for item in pairs:
    body = {"Bill": {"TxnDate": str(date.today()), "DueDate": str(date.today() + relativedelta(months=1))}, "Line": [{"DetailType": "AccountBasedExpenseLineDetail","Amount": item[1],"Id": "0","AccountBasedExpenseLineDetail": 
        {"AccountRef": { "value": "7"}} }], "VendorRef": { "value": item[0]}}
    print(json.dumps(body))
