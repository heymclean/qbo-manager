import requests
import json
import random, ast
from datetime import date
from dateutil.relativedelta import relativedelta
from django.conf import settings

def qbo_api_call(access_token, realm_id):
    
    if settings.ENVIRONMENT == 'production':
        base_url = settings.QBO_BASE_PROD
    else:
        base_url =  settings.QBO_BASE_SANDBOX

    route = '/v3/company/{0}/companyinfo/{0}'.format(realm_id)
    auth_header = 'Bearer {0}'.format(access_token)
    headers = {
        'Authorization': auth_header, 
        'Accept': 'application/json'
    }
    return requests.get('{0}{1}'.format(base_url, route), headers=headers)

def qbo_get_bills(access_token, realm_id):
    
    if settings.ENVIRONMENT == 'production':
        base_url = settings.QBO_BASE_PROD
    else:
        base_url =  settings.QBO_BASE_SANDBOX

    route = '/v3/company/{0}/query?query=select * from bill maxresults 200'.format(realm_id)
    auth_header = 'Bearer {0}'.format(access_token)
    headers = {
        'Authorization': auth_header, 
        'Accept': 'application/json'
    }
    result = requests.get('{0}{1}'.format(base_url, route), headers=headers)
    resultJSON = json.dumps(result.text)
    resultContent = json.loads(result.content.decode('utf-8'))
    count = 0
    for item in resultContent['QueryResponse']['Bill']:
        count +=1

    print(count)
    #f1 = open("cache1.txt", "w")
    #f1.write(result.text)

    return result

def qbo_delete_stale_bills(access_token, realm_id):
    
    if settings.ENVIRONMENT == 'production':
        base_url = settings.QBO_BASE_PROD
    else:
        base_url =  settings.QBO_BASE_SANDBOX

    #stale = open('SampleOAuth2_UsingPythonClient-master\storage\billIDs.json', 'r')
    #stale = stale.read()
    #stale = json.loads(stale)

    route = '/v3/company/{0}/bill/'.format(realm_id)
    route2 = '/v3/company/{0}/bill'.format(realm_id)
    auth_header = 'Bearer {0}'.format(access_token)
    headers = {
        'Authorization': auth_header,
        'Accept': 'application/json'
    }

    headers2 = {
        'Authorization': auth_header,
        'Content-type': 'application/json'
    }

    f = open('./storage/billIDs.json', 'r')
    f = f.read()
    f = json.loads(f)

    count = 0
    for item in f['staleBills']:
        result = requests.get('{0}{1}{2}'.format(base_url, route, item), headers=headers)
        count += 1
        result1 = json.loads(result.text, encoding='utf-8')
        print(result1["Bill"]["DueDate"])
        result1["Bill"]["DueDate"] = str(date.today() + relativedelta(months=1))
        result1["Bill"]["TxnDate"] = str(date.today())
        print(result1["Bill"]["DueDate"])
        result1 = result1["Bill"]
        result2 = json.dumps(result1)
        print(result2)
        result3 = requests.post('{0}{1}'.format(base_url, route2), headers=headers2, data=result2)
    print(count)

    return result3

def qbo_get_vendors(access_token, realm_id):
    
    if settings.ENVIRONMENT == 'production':
        base_url = settings.QBO_BASE_PROD
    else:
        base_url =  settings.QBO_BASE_SANDBOX

    route = '/v3/company/{0}/query?query=select * from vendor'.format(realm_id)
    auth_header = 'Bearer {0}'.format(access_token)
    headers = {
        'Authorization': auth_header, 
        'Accept': 'application/json'
    }

    result = requests.get('{0}{1}'.format(base_url, route), headers=headers)
    resultContent = json.loads(result.content.decode('utf-8'))

    vendorIDs = []
    for item in resultContent['QueryResponse']['Vendor']:
        vendorIDs.append(item['Id'])
        print(item['DisplayName'], item['Id'])

    print(vendorIDs)
    f = open("./storage/vendorIDs.json", "w", encoding = "utf8")  
    f.write(str(vendorIDs))

    return result

def qbo_create_fresh_bills(access_token, realm_id):
    
    # TO DO: Create a system to create more bills. Figure out if need to increment ID, what info is minimum, etc. :D 

    if settings.ENVIRONMENT == 'production':
        base_url = settings.QBO_BASE_PROD
    else:
        base_url =  settings.QBO_BASE_SANDBOX

    route = '/v3/company/{0}/bill'.format(realm_id)
    auth_header = 'Bearer {0}'.format(access_token)
    headers = {
        'Authorization': auth_header, 
        'Content-type': 'application/json'
    }

    vendors = open("C:/Users/Graham/Documents/Python/qbo/SampleOAuth2_UsingPythonClient-master/storage/vendorIDs.json", "r")
    vendorList = ast.literal_eval(vendors.read())
    amountList = []
    for item in vendorList:
        amountList.append(str(random.randrange(199,3999)))

    pairs = list(zip(vendorList, amountList))

    count = 0
    for item in pairs:
        body = {"TxnDate": str(date.today()), "DueDate": str(date.today() + relativedelta(months=1)), "Line": [{"DetailType": "AccountBasedExpenseLineDetail","Amount": item[1],"Id": "0","AccountBasedExpenseLineDetail": 
            {"AccountRef": { "value": "7"}} }], "VendorRef": { "value": item[0]}}
        body = json.dumps(body)
        print(body)
        result = requests.post('{0}{1}'.format(base_url, route), headers=headers, data=body)

    print(count)
    return result