import json
from datetime import date, datetime

today = datetime.today() 

""" a = open("cache1.txt", "r")
b = a.read() 
c = json.loads(b) """

def countBills(jsonInput): 
    jsonInput = json.loads(jsonInput)
    # An active bill has: a) A due date after today, and b) a positive balance.
    activeBills = 0
    activeBillsList = []
    
    # A stale bill has: a) a due date before today, and b) a positive balance.
    staleBills = 0 
    staleBillsList = []

    # A close bill has: a) no balance. 
    closedBills = 0 
    closedBillsList = []

    for item in jsonInput['QueryResponse']['Bill']: 
        if (item['Balance'] > 0): 
            if (datetime.strptime(item['DueDate'],'%Y-%m-%d') > today):
                activeBills += 1
                activeBillsList.append(item['Id'])
            else: 
                staleBills += 1
                staleBillsList.append(item['Id'])
        else: 
            closedBills += 1
            closedBillsList.append(item['Id'])

    print("---------------------")
    print("Active Bills: ", activeBills)
    print("         Ids:", activeBillsList)
    print("---------------------")
    print("Stale Bills: ", staleBills)
    print("        Ids:", staleBillsList)
    print("---------------------")
    print("Closed Bills: ", closedBills)
    print("        Ids:", closedBillsList)
    print("---------------------")
    print("Total Count:", activeBills+staleBills+closedBills)

    result = {"activeBills": activeBillsList, "staleBills": staleBillsList, "closedBills": closedBillsList}

    return result

def del_bills(): 
    f = open('storage/billIDs.json', 'r')
    f = f.read()
    f = json.loads(f)

    for item in f['staleBills']:
        body = {"SyncToken": "0", "Id": item}
        print(body)